﻿using I2SE_Assignment2MVCApp.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I2SE_Assignment2JSONCreatorApp
{
    class Program
    {
        static void Main(string[] args)
        {
            // Create course-registration relationships
            List<string> emptyStrings = new List<string>();
            Registration reg1 = new Registration() { CourseID = "MAT101", StudentIDs = emptyStrings };
            Registration reg2 = new Registration() { CourseID = "MAT102", StudentIDs = emptyStrings };
            Registration reg3 = new Registration() { CourseID = "CHE101", StudentIDs = emptyStrings };
            Registration reg4 = new Registration() { CourseID = "PHY101", StudentIDs = emptyStrings };
            Registration reg5 = new Registration() { CourseID = "ENG101", StudentIDs = emptyStrings };
            Registration reg6 = new Registration() { CourseID = "HIS101", StudentIDs = emptyStrings };
            Registration reg7 = new Registration() { CourseID = "MEC101", StudentIDs = emptyStrings };
            Registration reg8 = new Registration() { CourseID = "CME101", StudentIDs = emptyStrings };

            // Create lists
            List<Student> students = new List<Student>();
            List<Professor> professors = new List<Professor>();
            List<Course> courses = new List<Course>();
            List<Registration> emptyRegis = new List<Registration>();
            List<Registration> registrations = new List<Registration>();

            // Populate student list
            students.Add(new Student() { ID = "ST001", Name = "Alice", Gender = "Female", Password = "pass1234", Registrations = emptyRegis });
            students.Add(new Student() { ID = "ST002", Name = "Bob", Gender = "Male", Password = "pass1234", Registrations = emptyRegis });
            students.Add(new Student() { ID = "ST003", Name = "Christopher", Gender = "Male", Password = "pass1234", Registrations = emptyRegis });
            students.Add(new Student() { ID = "ST004", Name = "Dory", Gender = "Female", Password = "pass1234", Registrations = emptyRegis });
            students.Add(new Student() { ID = "ST005", Name = "Elizabeth", Gender = "Female", Password = "pass1234", Registrations = emptyRegis });
            students.Add(new Student() { ID = "ST006", Name = "Fred", Gender = "Male", Password = "pass1234", Registrations = emptyRegis });
            students.Add(new Student() { ID = "ST007", Name = "Gary", Gender = "Male", Password = "pass1234", Registrations = emptyRegis });
            students.Add(new Student() { ID = "ST008", Name = "Harry", Gender = "Male", Password = "pass1234", Registrations = emptyRegis });
            students.Add(new Student() { ID = "ST009", Name = "Isabella", Gender = "Female", Password = "pass1234", Registrations = emptyRegis });
            students.Add(new Student() { ID = "ST010", Name = "Justin", Gender = "Male", Password = "pass1234", Registrations = emptyRegis });
            students.Add(new Student() { ID = "ST011", Name = "Kelly", Gender = "Female", Password = "pass1234", Registrations = emptyRegis });
            students.Add(new Student() { ID = "ST012", Name = "Lucy", Gender = "Female", Password = "pass1234", Registrations = emptyRegis });
            students.Add(new Student() { ID = "ST013", Name = "Maria", Gender = "Female", Password = "pass1234", Registrations = emptyRegis });
            students.Add(new Student() { ID = "ST014", Name = "Nathan", Gender = "Male", Password = "pass1234", Registrations = emptyRegis });
            students.Add(new Student() { ID = "ST015", Name = "Oprah", Gender = "Female", Password = "pass1234", Registrations = emptyRegis });
            students.Add(new Student() { ID = "ST016", Name = "Peter", Gender = "Male", Password = "pass1234", Registrations = emptyRegis });

            // Populate professor list
            professors.Add(new Professor() { ID = "PR001", Name = "Prof. Isabella", Gender = "Female", Password = "pass1234", Registrations = emptyRegis });
            professors.Add(new Professor() { ID = "PR002", Name = "Prof. Justin", Gender = "Male", Password = "pass1234", Registrations = emptyRegis });
            professors.Add(new Professor() { ID = "PR003", Name = "Prof. Kelly", Gender = "Female", Password = "pass1234", Registrations = emptyRegis });
            professors.Add(new Professor() { ID = "PR004", Name = "Prof. Lucy", Gender = "Female", Password = "pass1234", Registrations = emptyRegis });
            professors.Add(new Professor() { ID = "PR005", Name = "Prof. Maria", Gender = "Female", Password = "pass1234", Registrations = emptyRegis });
            professors.Add(new Professor() { ID = "PR006", Name = "Prof. Nathan", Gender = "Male", Password = "pass1234", Registrations = emptyRegis });
            professors.Add(new Professor() { ID = "PR007", Name = "Prof. Oprah", Gender = "Female", Password = "pass1234", Registrations = emptyRegis });
            professors.Add(new Professor() { ID = "PR008", Name = "Prof. Peter", Gender = "Male", Password = "pass1234", Registrations = emptyRegis });

            // Populate course list
            courses.Add(new Course() { ID = "MAT101", Name = "Calculus 1", Department = "Mathematics", Cost = 99.99, Registration = reg1 });
            courses.Add(new Course() { ID = "MAT102", Name = "Calculus 2", Department = "Mathematics", Cost = 199.99, Registration = reg2 });
            courses.Add(new Course() { ID = "CHE101", Name = "Chemistry 1", Department = "Science", Cost = 299.99, Registration = reg3 });
            courses.Add(new Course() { ID = "PHY101", Name = "Physics 1", Department = "Science", Cost = 299.99, Registration = reg4 });
            courses.Add(new Course() { ID = "ENG101", Name = "English 1", Department = "Humanities", Cost = 49.99, Registration = reg5 });
            courses.Add(new Course() { ID = "HIS101", Name = "World History 1", Department = "Humanities", Cost = 69.99, Registration = reg6 });
            courses.Add(new Course() { ID = "MEC101", Name = "Mechanical Engineering 1", Department = "Engineering", Cost = 499.99, Registration = reg7 });
            courses.Add(new Course() { ID = "CME101", Name = "Chemical Engineering 1", Department = "Engineering", Cost = 499.99, Registration = reg8 });

            // Populate registration list
            registrations.Add(reg1);
            registrations.Add(reg2);
            registrations.Add(reg3);
            registrations.Add(reg4);
            registrations.Add(reg5);
            registrations.Add(reg6);
            registrations.Add(reg7);
            registrations.Add(reg8);

            // Write files
            string directory = @"Data/";
            SerializeJSON(directory + @"Students.json", students);
            SerializeJSON(directory + @"Professors.json", professors);
            SerializeJSON(directory + @"Courses.json", courses);
            SerializeJSON(directory + @"Registrations.json", registrations);

            foreach (var student in students)
            {
                SerializeJSON(directory + @"Student/" + student.ID + ".json", student);
            }
            foreach (var professor in professors)
            {
                SerializeJSON(directory + @"Professor/" + professor.ID + ".json", professor);
            }
            foreach (var course in courses)
            {
                SerializeJSON(directory + @"Course/" + course.ID + ".json", course);
            }
            foreach (var registration in registrations)
            {
                SerializeJSON(directory + @"Registration/" + registration.CourseID + ".json", registration);
            }
        }

        static void SerializeJSON<T>(string path, List<T> list)
        {
            using (FileStream fs = File.Open(path, FileMode.Create))
            using (StreamWriter sw = new StreamWriter(fs))
            using (JsonWriter jw = new JsonTextWriter(sw))
            {
                jw.Formatting = Formatting.Indented;

                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(jw, list.ToArray());
            }
        }

        static void SerializeJSON(string path, object obj)
        {
            using (FileStream fs = File.Open(path, FileMode.Create))
            using (StreamWriter sw = new StreamWriter(fs))
            using (JsonWriter jw = new JsonTextWriter(sw))
            {
                jw.Formatting = Formatting.Indented;

                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(jw, obj);
            }
        }
    }
}
