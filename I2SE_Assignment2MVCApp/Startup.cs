﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(I2SE_Assignment2MVCApp.Startup))]
namespace I2SE_Assignment2MVCApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
