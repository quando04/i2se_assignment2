﻿using System.Web;
using System.Web.Mvc;

namespace I2SE_Assignment2MVCApp
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
