﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Web;

namespace I2SE_Assignment2MVCApp.Models
{
    public abstract class Member
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }
        public string Password { get; set; }
        public List<Registration> Registrations { get; set; }
    }

    public class Student : Member
    {
    }

    public class Professor : Member
    {
    }

    public class Course
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string Department { get; set; }
        public double Cost { get; set; }
        public Registration Registration { get; set; }
    }

    public class Registration
    {
        [Display(Name = "Course ID")]
        public string CourseID { get; set; }
        [Display(Name = "Professor ID")]
        public string ProfessorID { get; set; }
        public List<string> StudentIDs { get; set; }
        public Course Course { get; set; }
        public Professor Professor { get; set; }
        public List<Student> Students { get; set; }
    }

    public class School
    {
        public static Member User { get; set; }
        public static string[] Role { get; set; }

        private static string directory = HttpRuntime.AppDomainAppPath + "Data";

        public static Student[] GetAllStudents()
        {
            return DeserializeJsonFromFile<Student[]>("Students.json");
        }

        public static Professor[] GetAllProfessors()
        {
            return DeserializeJsonFromFile<Professor[]>("Professors.json");
        }

        public static Course[] GetAllCourses()
        {
            return DeserializeJsonFromFile<Course[]>("Courses.json");
        }

        public static Registration[] GetAllRegis()
        {
            return DeserializeJsonFromFile<Registration[]>("Registrations.json");
        }

        public static Student GetStudent(string studentID)
        {
            string file = @"Student/" + studentID + ".json";
            return DeserializeJsonFromFile<Student>(file);
        }
        public static Professor GetProfessor(string professorID)
        {
            string file = @"Professor/" + professorID + ".json";
            return DeserializeJsonFromFile<Professor>(file);
        }

        public static Course GetCourse(string courseID)
        {
            string file = @"Course/" + courseID + ".json";
            return DeserializeJsonFromFile<Course>(file);
        }

        public static Registration GetRegistration(string courseID)
        {
            string file = @"Registration/" + courseID + ".json";
            return DeserializeJsonFromFile<Registration>(file);
        }

        public static void Register(Registration newReg)
        {
            // Edit Registration instance
            SerializeJSON(Path.Combine(directory, "Registration", newReg.CourseID + ".json"), newReg);
            // Edit Registration property of the course
            var course = GetCourse(newReg.CourseID);
            course.Registration = newReg;
            SerializeJSON(Path.Combine(directory, "Course", newReg.CourseID + ".json"), course);
            if (Role.Contains("Professor"))
            {
                // Edit Registrations property of the professor who registered
                if (newReg.ProfessorID != null)
                {
                    var professor = GetProfessor(newReg.ProfessorID);
                    professor.Registrations.Add(newReg);
                    SerializeJSON(Path.Combine(directory, "Professor", newReg.ProfessorID + ".json"), professor);
                }
                // Edit Registrations property of all students taking the course this professor teaches
                foreach (string studentID in newReg.StudentIDs)
                {
                    var student = GetStudent(studentID);
                    foreach (var item in student.Registrations)
                    {
                        if (item.CourseID.Equals(newReg.CourseID))
                        {
                            item.ProfessorID = newReg.ProfessorID;
                            SerializeJSON(Path.Combine(directory, "Student", studentID + ".json"), student);
                            break;
                        }
                    }
                }
            }
            else if (Role.Contains("Student"))
            {
                // Edit Registrations property of the student who registers
                if (newReg.StudentIDs != null)
                {
                    var student = GetStudent(newReg.StudentIDs.Last());
                    student.Registrations.Add(newReg);
                    SerializeJSON(Path.Combine(directory, "Student", newReg.StudentIDs.Last() + ".json"), student);
                }
                // Edit Registrations property of all other students in the class
                for (int i = 0; i < newReg.StudentIDs.Count - 1; i++)
                {
                    var student = GetStudent(newReg.StudentIDs.ElementAt(i));
                    foreach (var item in student.Registrations)
                    {
                        if (item.CourseID.Equals(newReg.CourseID))
                        {
                            item.StudentIDs.Add(newReg.StudentIDs.Last());
                            SerializeJSON(Path.Combine(directory, "Student", student.ID + ".json"), student);
                            break;
                        }
                    }
                }
                // Edit Registrations property of the professor teaching the course this student signs up
                if (newReg.ProfessorID != null)
                {
                    var professor = GetProfessor(newReg.ProfessorID);
                    foreach (var item in professor.Registrations)
                    {
                        if (item.CourseID.Equals(newReg.CourseID))
                        {
                            item.StudentIDs.Add(newReg.StudentIDs.Last());
                            SerializeJSON(Path.Combine(directory, "Professor", newReg.ProfessorID + ".json"), professor);
                            break;
                        }
                    }
                }
            }
        }

        public static void Drop(Registration newReg)
        {
            // Edit Registration instance
            SerializeJSON(Path.Combine(directory, "Registration", newReg.CourseID + ".json"), newReg);
            // Edit Registration property of the course
            var course = GetCourse(newReg.CourseID);
            course.Registration = newReg;
            SerializeJSON(Path.Combine(directory, "Course", newReg.CourseID + ".json"), course);
            if (Role.Contains("Professor"))
            {
                // Edit Registrations property of the professor who dropped
                var professor = GetProfessor(User.ID);
                foreach (var item in professor.Registrations)
                {
                    if (item.CourseID.Equals(newReg.CourseID))
                    {
                        professor.Registrations.Remove(item);
                        SerializeJSON(Path.Combine(directory, "Professor", User.ID + ".json"), professor);
                        break;
                    }
                }
                // Edit Registrations property of all students taking the course this professor dropped
                foreach (var studentID in newReg.StudentIDs)
                {
                    var student = GetStudent(studentID);
                    foreach (var item in student.Registrations)
                    {
                        if (item.CourseID.Equals(newReg.CourseID))
                        {
                            item.ProfessorID = null;
                            SerializeJSON(Path.Combine(directory, "Student", student.ID + ".json"), student);
                            break;
                        }
                    }
                }
            }
            else if (Role.Contains("Student"))
            {
                if (newReg.StudentIDs != null)
                {
                    // Edit Registrations property of the student who drops
                    var student = GetStudent(User.ID);
                    foreach (var item in student.Registrations)
                    {
                        if (item.CourseID.Equals(newReg.CourseID))
                        {
                            student.Registrations.Remove(item);
                            SerializeJSON(Path.Combine(directory, "Student", student.ID + ".json"), student);
                            break;
                        }
                    }
                    // Edit Registrations property of all other students in the class
                    foreach (var studentID in newReg.StudentIDs)
                    {
                        var std = GetStudent(studentID);
                        foreach (var item in std.Registrations)
                        {
                            if (item.CourseID.Equals(newReg.CourseID))
                            {
                                item.StudentIDs.Remove(User.ID);
                                SerializeJSON(Path.Combine(directory, "Student", std.ID + ".json"), std);
                                break;
                            }
                        }
                    }
                }
                // Edit Registrations property of the professor teaching the course this student drops
                if (newReg.ProfessorID != null)
                {
                    var professor = GetProfessor(newReg.ProfessorID);
                    foreach (var item in professor.Registrations)
                    {
                        if (item.CourseID.Equals(newReg.CourseID))
                        {
                            item.StudentIDs.Remove(User.ID);
                            SerializeJSON(Path.Combine(directory, "Professor", professor.ID + ".json"), professor);
                            break;
                        }
                    }
                }
            }
        }

        private static T DeserializeJsonFromFile<T>(string file)
        {
            using (StreamReader sr = File.OpenText(Path.Combine(directory, file)))
            {
                JsonSerializer serializer = new JsonSerializer();
                return (T)serializer.Deserialize(sr, typeof(T));
            }
        }

        private static void SerializeJSON(string path, object obj)
        {
            using (FileStream fs = File.Open(path, FileMode.Create))
            using (StreamWriter sw = new StreamWriter(fs))
            using (JsonWriter jw = new JsonTextWriter(sw))
            {
                jw.Formatting = Formatting.Indented;

                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(jw, obj);
            }
        }
    }
}