﻿using I2SE_Assignment2MVCApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace I2SE_Assignment2MVCApp.Controllers
{
    public class ProfessorsController : Controller
    {
        // GET: Professors/Details/5
        public ActionResult Details(string id)
        {
            return View(School.GetProfessor(id));
        }
    }
}
