﻿using I2SE_Assignment2MVCApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace I2SE_Assignment2MVCApp.Controllers
{
    public class CoursesController : Controller
    {
        // GET: Courses
        // View course catalog
        public ActionResult Index()
        {
            return View(School.GetAllCourses());
        }

        // GET: Courses/Details/5
        // View details of a course
        public ActionResult Details(string id)
        {
            return View(School.GetCourse(id));
        }
    }
}
