﻿using I2SE_Assignment2MVCApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace I2SE_Assignment2MVCApp.Controllers
{
    public class RegistrationController : Controller
    {
        // GET: Registration/Index
        // View user's current courses
        public ActionResult Index()
        {
            if (School.User != null)
            {
                ICollection<Registration> myCourses = new List<Registration>();
                if (School.User is Professor)
                {
                    foreach (var item in School.GetProfessor(School.User.ID).Registrations)
                    {
                        myCourses.Add(item);
                    }
                }
                else
                {
                    foreach (var item in School.GetStudent(School.User.ID).Registrations)
                    {
                        myCourses.Add(item);
                    }
                }
                return View(myCourses);
            }
            // Has not logged in
            return RedirectToAction("Login", "Account");
        }

        // GET: Registration/Register/5
        // User registers for a course  
        public ActionResult Register(string id)
        {
            if (School.User != null)
            {
                var newReg = School.GetRegistration(id);

                if (School.Role.Contains("Student"))
                {
                    // Cannot sign up twice
                    if (newReg.StudentIDs.Contains(School.User.ID))
                    {
                        TempData["ErrorMsg"] = "You have already registered for this course.";
                        return RedirectToAction("Index", "Courses");
                    }
                    // Maximum 10 students in course
                    if (newReg.StudentIDs.Count >= 10)
                    {
                        TempData["ErrorMsg"] = "Only a maximum of 10 students are allowed.";
                        return RedirectToAction("Index", "Courses");
                    }
                    // Maximum 4 courses/semester (1 registration/course)
                    if (School.GetStudent(School.User.ID).Registrations.Count >= 4)
                    {
                        TempData["ErrorMsg"] = "You may only register for 4 courses per semester.";
                        return RedirectToAction("Index", "Courses");
                    }

                    newReg.StudentIDs.Add(School.User.ID);
                    // Write to files
                    School.Register(newReg);
                    TempData["SuccessMsg"] = "Registration Successful!";
                    return RedirectToAction("Index");
                }
                else if (School.Role.Contains("Professor"))
                {
                    // Course already has a professor
                    if (newReg.ProfessorID != null)
                    {
                        // The professor is the user
                        if (newReg.ProfessorID.Equals(School.User.ID))
                        {
                            TempData["ErrorMsg"] = "You have already registered to teach this course.";
                        }
                        // The professor is not the user
                        else
                        {
                            TempData["ErrorMsg"] = "Another professor has already registered to teach this course.";
                        }
                        return RedirectToAction("Index", "Courses");
                    }

                    newReg.ProfessorID = School.User.ID;
                    // Write to files
                    School.Register(newReg);
                    TempData["SuccessMsg"] = "Registration Successful!";
                    return RedirectToAction("Index");
                }
            }
            // Has not logged in
            TempData["ErrorMsg"] = "You must log in to register for a course.";
            return RedirectToAction("Index", "Courses");
        }

        // GET: Registration/Drop/5
        // User drops a course  
        public ActionResult Drop(string id)
        {
            if (School.User != null)
            {
                var newReg = School.GetRegistration(id);

                if (School.Role.Contains("Student"))
                {
                    if (!newReg.StudentIDs.Contains(School.User.ID))
                    {
                        TempData["ErrorMsg"] = "You have not signed up for the chosen course.";
                        return RedirectToAction("Index");
                    }
                    newReg.StudentIDs.Remove(School.User.ID);
                    // Write to files
                    School.Drop(newReg);
                    TempData["SuccessMsg"] = "Course Drop Successful!";
                    return RedirectToAction("Index");
                }
                else if (School.Role.Contains("Professor"))
                {
                    if (newReg.ProfessorID == null)
                    {
                        TempData["ErrorMsg"] = "You have not signed up to teach the chosen course.";
                        return RedirectToAction("Index");
                    }
                    newReg.ProfessorID = null;
                    // Write to files
                    School.Drop(newReg);
                    TempData["SuccessMsg"] = "Course Drop Successful!";
                    return RedirectToAction("Index");
                }
            }
            // Has not logged in
            TempData["ErrorMsg"] = "You must log in to register for a course.";
            return RedirectToAction("Index", "Courses");
        }

        // GET: Registration/Students/5
        // Professor views all students who registered for his course offerings
        public ActionResult Students(string id)
        {
            ICollection<Student> myStudents = new List<Student>();
            var reg = School.GetRegistration(id);
            foreach (var studentID in reg.StudentIDs)
            {
                myStudents.Add(School.GetStudent(studentID));
            }
            ViewBag.Course = School.GetCourse(reg.CourseID);
            return View(myStudents);
        }

        // GET: Registration/Check
        // Student view billing info
        public ActionResult Check()
        {
            if (School.User != null)
            {
                ICollection<Course> myCourses = new List<Course>();
                if (School.User is Student)
                {
                    foreach (var item in School.GetStudent(School.User.ID).Registrations)
                    {
                        myCourses.Add(School.GetCourse(item.CourseID));
                    }
                }
                return View(myCourses);
            }
            // Has not logged in as Student
            TempData["ErrorMsg"] = "You must sign in as a student to continue.";
            return RedirectToAction("Index");
        }
    }
}
